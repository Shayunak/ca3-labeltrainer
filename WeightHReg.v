module WeightHReg(offset ,weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9,
	weight10, weight11, weight12, weight13, weight14, weight15, weight16, weight17, weight18, weight19);
	input[5:0] offset;	
	reg[7:0]mem[0:1239];
	output[7:0] weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9,
	weight10, weight11, weight12, weight13, weight14, weight15, weight16, weight17, weight18, weight19;
	//
	initial begin
		$readmemh("./wh_sm.dat", mem);
	end
	//
	assign weight0 = mem[offset];
	assign weight1 = mem[offset + 62];
	assign weight2 = mem[offset + 124];
	assign weight3 = mem[offset + 186];
	assign weight4 = mem[offset + 248];
	assign weight5 = mem[offset + 310];
	assign weight6 = mem[offset + 372];
	assign weight7 = mem[offset + 434];
	assign weight8 = mem[offset + 496];
	assign weight9 = mem[offset + 558];
	assign weight10 = mem[offset + 620];
	assign weight11 = mem[offset + 682];
	assign weight12 = mem[offset + 744];
	assign weight13 = mem[offset + 806];
	assign weight14 = mem[offset + 868];
	assign weight15 = mem[offset + 930];
	assign weight16 = mem[offset + 992];
	assign weight17 = mem[offset + 1054];
	assign weight18 = mem[offset + 1116];
	assign weight19 = mem[offset + 1178];
endmodule
