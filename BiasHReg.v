module BiasHReg(bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9,
	bias10, bias11, bias12, bias13, bias14, bias15, bias16, bias17, bias18, bias19);
	reg[7:0]mem[0:19];
	output[7:0] bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9,
	bias10, bias11, bias12, bias13, bias14, bias15, bias16, bias17, bias18, bias19;
	//
	initial begin
		$readmemh("./bh_sm.dat", mem);
	end
	//
	assign bias0 = mem[0];
	assign bias1 = mem[1];
	assign bias2 = mem[2];
	assign bias3 = mem[3];
	assign bias4 = mem[4];
	assign bias5 = mem[5];
	assign bias6 = mem[6];
	assign bias7 = mem[7];
	assign bias8 = mem[8];
	assign bias9 = mem[9];
	assign bias10 = mem[10];
	assign bias11 = mem[11];
	assign bias12 = mem[12];
	assign bias13 = mem[13];
	assign bias14 = mem[14];
	assign bias15 = mem[15];
	assign bias16 = mem[16];
	assign bias17 = mem[17];
	assign bias18 = mem[18];
	assign bias19 = mem[19];
endmodule
