module saturation #(parameter m = 21)(in, out);
  input [m:0] in;
  output [7:0] out;
  
  assign  out = (in > 127) ? 8'b01111111 : in;
endmodule
