module Hreg(clk, rst, load1 , load2, offset,
	data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, out);
	input clk,rst,load1,load2;
	input[4:0] offset;
	input[7:0] data0, data1, data2, data3, data4, data5, data6, data7, data8, data9;
	output[7:0] out;
	reg [7:0]mem[0:19];
	always@(posedge rst , posedge clk) begin
		if(rst) begin
			mem[0] <= 0;	mem[1] <= 0;	mem[2] <= 0;	mem[3] <= 0;
			mem[4] <= 0;	mem[5] <= 0;	mem[6] <= 0;	mem[7] <= 0;
			mem[8] <= 0;	mem[9] <= 0;	mem[10] <= 0;	mem[11] <= 0;
			mem[12] <= 0;	mem[13] <= 0;	mem[14] <= 0;	mem[15] <= 0;
			mem[16] <= 0;	mem[17] <= 0;	mem[18] <= 0;	mem[19] <= 0;
		end
		else if(load1) begin
			mem[0] <= data0;mem[1] <= data1;mem[2] <= data2;mem[3] <= data3;
			mem[4] <= data4;mem[5] <= data5;mem[6] <= data6;mem[7] <= data7;
			mem[8] <= data8;mem[9] <= data9;
		end else if(load2) begin
			mem[10] <= data0;mem[11] <= data1;mem[12] <= data2;mem[13] <= data3;
			mem[14] <= data4;mem[15] <= data5;mem[16] <= data6;mem[17] <= data7;
			mem[18] <= data8;mem[19] <= data9;
		end
	end
	assign out = mem[offset];
endmodule
