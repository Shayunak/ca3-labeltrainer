module accRegister#(parameter size = 18)(ld , inp, clk , rst, clr, out);
  input [size:0]inp;
  input rst,clk, clr,ld;
  output reg [size:0] out;
  
  always@(posedge clk, posedge rst) begin
    if(rst) out <= 0;
    else if (clr) out <= 0;
    else if(ld) out <= inp;
  end
  
endmodule