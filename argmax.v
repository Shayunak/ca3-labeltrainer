module argmax(data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, arg);
	// Initialization	
	output[3:0] arg;
	input[7:0] data0, data1, data2, data3, data4, data5, data6, data7, data8, data9;
	wire[3:0] indexPass1, indexPass2, indexPass3, indexPass4, indexPass5, indexPass6, indexPass7, indexPass8;
	wire[7:0] maxPass1, maxPass2, maxPass3, maxPass4, maxPass5, maxPass6, maxPass7, maxPass8, maxPass9;
	wire compRes1, compRes2, compRes3, compRes4, compRes5, compRes6, compRes7, compRes8, compRes9;
	// Pass 1
	assign compRes1 = data1 > data0;
	assign maxPass1 = compRes1 ? data1 : data0;
	assign indexPass1 = compRes1 ? 4'b0001 : 4'b0000;
	// Pass 2
	assign compRes2 = data2 > maxPass1;
	assign maxPass2 = compRes2 ? data2 : maxPass1;
	assign indexPass2 = compRes2 ? 4'b0010 : indexPass1;
	// Pass 3
	assign compRes3 = data3 > maxPass2;
	assign maxPass3 = compRes3 ? data3 : maxPass2;
	assign indexPass3 = compRes3 ? 4'b0011 : indexPass2;
	// Pass 4
	assign compRes4 = data4 > maxPass3;
	assign maxPass4 = compRes4 ? data4 : maxPass3;
	assign indexPass4 = compRes4 ? 4'b0100 : indexPass3;
	// Pass 5
	assign compRes5 = data5 > maxPass4;
	assign maxPass5 = compRes5 ? data5 : maxPass4;
	assign indexPass5 = compRes5 ? 4'b0101 : indexPass4;
	// Pass 6
	assign compRes6 = data6 > maxPass5;
	assign maxPass6 = compRes6 ? data6 : maxPass5;
	assign indexPass6 = compRes6 ? 4'b0110 : indexPass5;
	// Pass 7
	assign compRes7 = data7 > maxPass6;
	assign maxPass7 = compRes7 ? data7 : maxPass6;
	assign indexPass7 = compRes7 ? 4'b0111 : indexPass6;
	// Pass 8
	assign compRes8 = data8 > maxPass7;
	assign maxPass8 = compRes8 ? data8 : maxPass7;
	assign indexPass8 = compRes8 ? 4'b1000 : indexPass7;
	// Pass 9
	assign compRes9 = data9 > maxPass8;
	assign maxPass9 = compRes9 ? data9 : maxPass8;
	assign arg = compRes9 ? 4'b1001 : indexPass8;
endmodule
