module finalTB();
	integer i;
	integer correct;
	reg system_start,clk,rst;
	wire system_ready;
	reg[9:0] dataset_select;
	wire[3:0] outNetwork;
	reg[7:0]labels[0:749];
	NeuralNetwork NN (system_start, system_ready, clk , rst , outNetwork, dataset_select);
	initial begin
		correct = 0;
		system_start = 1'b0;
		rst = 1'b0;
    		clk = 1'b0;
		#200
		rst = 1'b1;
		#200
		rst = 1'b0;
    		repeat(250000) #100 clk = ~clk;
		$stop;  
  	end
	initial begin
		$readmemh("./test_lable_sm.dat", labels);
		#800
		for(i = 0 ; i < 750 ; i = i + 1) begin
		  //i = 5;
			dataset_select = i;
			system_start = 1'b1;
			#200
			system_start = 1'b0;
			#31000
			if(outNetwork == labels[i] ) correct = correct + 1;
		end
		#800
		$display("Number of Correct Prediction: %d", correct);
		$stop;
	end
endmodule
