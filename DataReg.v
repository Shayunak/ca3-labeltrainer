module DataReg(dataset_select, offset, data);
	output[7:0] data;
	input[5:0] offset;
	input[9:0] dataset_select;
	reg[7:0]mem[0:46499];
	initial begin
		$readmemh("./test_data_sm.dat", mem);
	end
	assign data = mem[dataset_select*62 + offset];
endmodule
