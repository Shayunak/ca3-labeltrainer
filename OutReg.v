module OutReg(out , parIn , ld , clk, rst);
	input[3:0]parIn;
	input ld,clk,rst;
	output reg[3:0] out;
	always @(posedge clk, posedge rst) begin
		if(rst) out <= 4'b0000;
		else if(ld) out <= parIn;
		else out <= out;
	end
endmodule
