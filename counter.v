module counter(loadc, parIn, val, cout, rst, clk);
	output reg[5:0] val;
	output cout;
	input[5:0] parIn;
	input loadc,rst,clk;
	always@(posedge clk, posedge rst) begin
		if(rst) val <= 0;
		else if(loadc) val <= parIn;
		else val <= val + 1;
	end
	assign cout = &val;
endmodule
