module BiasOReg(bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9);
	reg[7:0]mem[0:9];
	output[7:0] bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9;
	initial begin
		$readmemh("./bo_sm.dat", mem);
	end
	//
	assign bias0 = mem[0];
	assign bias1 = mem[1];
	assign bias2 = mem[2];
	assign bias3 = mem[3];
	assign bias4 = mem[4];
	assign bias5 = mem[5];
	assign bias6 = mem[6];
	assign bias7 = mem[7];
	assign bias8 = mem[8];
	assign bias9 = mem[9];
endmodule
