module SingleNeuron #(parameter accSize = 21)
	(clk, rst, weights, bios, data, out, clr, ldmac);
	
	input clk, rst, clr, ldmac;
	input [7:0]weights, bios;
	output [7:0]out;
	input [7:0]data;
	
	wire[15:0] multOut, shiftedBias;
	wire[accSize:0]adderIn, extendedBias;
	wire[accSize:0] accumulatorOut;
	wire[accSize:0] accumulatorIn;
	wire[accSize:0] shift9;
	wire[accSize:0] reluIn, reluOut;
	
	//former multiplier
	multiplier mult (.A(weights) , .B(data) , .out(multOut) );
	multiplier mult2 (.A(bios) , .B(8'b01111111) , .out(shiftedBias) );
	
	//sign extending
	assign adderIn = { {(accSize - 14){multOut[15]}} , multOut[14:0] };
	assign extendedBias = { {(accSize - 14){shiftedBias[15]}} , shiftedBias[14:0] };
	
	//add
	adder adr (.a(adderIn) , .b(accumulatorOut) , .s(accumulatorIn) );
	defparam adr.len = accSize;
	
	//accumulator
	accRegister Acc (.ld(ldmac) , .inp(accumulatorIn) , .clk(clk)  , .rst(rst) , .clr(clr) , .out(accumulatorOut) );
	defparam Acc.size = accSize;
	
	//add
	adder adr2 (.a(extendedBias) , .b(accumulatorOut) , .s(shift9) );
	defparam adr2.len = accSize;
			
	//assign reluIn = selectho ? shift9 : { {9{shift9[21]}} , shift9[21:9] };
	assign reluIn = { {9{shift9[21]}} , shift9[21:9] };
	//activationFunction
	activationFunction relu (.inp(reluIn) , .out(reluOut) );
	defparam relu.m = accSize;
  
  //saturation
  saturation sat(.in(reluOut), .out(out));
 	defparam sat.m = accSize;
endmodule
