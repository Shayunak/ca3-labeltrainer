module controller (clk, rst, system_start, system_ready, WH_offset, WO_offset, select_wh, select_ho, select_data, load1, load2, load_out, ldmac, clr);
  input system_start, clk, rst;
  output reg system_ready, select_wh, select_ho, select_data, load1, load2, load_out, ldmac, clr;
  output reg[5:0] WH_offset;
  output reg[4:0] WO_offset;
  wire[5:0] val;
  wire cout;
  reg loadc;
  reg[5:0] parIn;
  parameter idle= 0; parameter loading1= 1; parameter waiting1= 2; parameter saving1= 3; parameter loading2= 4;
  parameter waiting2= 5; parameter saving2= 6; parameter loading3= 7; parameter waiting3= 8; parameter saving3= 9;
  
  reg [3:0]ps;
  reg [3:0]ns;
  
  counter cnt (.loadc(loadc), .parIn(parIn), .val(val), .cout(cout), .rst(rst), .clk(clk) );

  always@(ps, val) begin
	{system_ready, select_wh, select_ho, select_data, load1, load2, load_out, ldmac, clr, loadc} <= 10'b0;
	{WO_offset,parIn} <= 11'b0;
	WH_offset <= 6'b0;
	case(ps)
		idle: system_ready <= 1'b1;
		loading1: begin {clr, loadc} <= 2'b11; parIn <= 6'b000010; end
		waiting1: begin {ldmac,select_wh,select_ho,select_data} <= 4'b1000; WH_offset <= val - 6'b000010;end
		saving1: load1 <= 1'b1;
		loading2: begin {clr, loadc} <= 2'b11; parIn <= 6'b000010; end
		waiting2: begin {ldmac,select_wh,select_ho,select_data} <= 4'b1100; WH_offset <= val - 6'b000010;end
		saving2: begin load2 <= 1'b1; select_wh <= 1'b1; end
		loading3: begin {clr, loadc} <= 2'b11; parIn <= 6'b101100; end
		waiting3: begin {ldmac,select_ho,select_data} <= 3'b111; WO_offset <= val - 6'b101100;end
		saving3: begin load_out <= 1'b1; select_ho <= 1'b1; end
	endcase
  end

    always@(ps, cout, system_start) begin
	case(ps)
		idle: ns <= system_start ? loading1 : idle;
		loading1: ns <= waiting1;
		waiting1: ns <= cout ? saving1 : waiting1;
		saving1: ns <= loading2;
		loading2: ns <= waiting2;
		waiting2: ns <= cout ? saving2 : waiting2;
		saving2: ns <= loading3;
		loading3: ns <= waiting3;
		waiting3: ns <= cout ? saving3 : waiting3;
		saving3: ns <= idle;
		default: ns <= idle;
	endcase
  end
  
  always@(posedge clk, posedge rst)begin
    if (rst) ps <= 0;
    else ps <= ns;
  end
    
endmodule
