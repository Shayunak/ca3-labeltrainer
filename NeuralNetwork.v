module NeuralNetwork(system_start, system_ready, clk , rst , outNetwork, dataset_select);
	input[9:0] dataset_select;
	input clk,rst,system_start;
	output[3:0] outNetwork;
	output system_ready;
	//Controller Initialization
	wire[4:0] WO_offset; wire[5:0] WH_offset;
	wire select_wh, select_ho, select_data, load1, load2, load_out, ldmac, clr;
	// Macs Hidden Layer Weight Input
	wire[7:0] FirstWeightH0, FirstWeightH1, FirstWeightH2, FirstWeightH3, FirstWeightH4, FirstWeightH5, FirstWeightH6, FirstWeightH7, FirstWeightH8, FirstWeightH9;
	wire[7:0] SecondWeightH0, SecondWeightH1, SecondWeightH2, SecondWeightH3, SecondWeightH4, SecondWeightH5, SecondWeightH6, SecondWeightH7, SecondWeightH8, SecondWeightH9;
	// Macs Output Layer Weight Input
	wire[7:0] WeightO0, WeightO1, WeightO2, WeightO3, WeightO4, WeightO5, WeightO6, WeightO7, WeightO8, WeightO9;
	// Macs Hidden Layer Bias Input
	wire[7:0] FirstBiasH0, FirstBiasH1, FirstBiasH2, FirstBiasH3, FirstBiasH4, FirstBiasH5, FirstBiasH6, FirstBiasH7, FirstBiasH8, FirstWBiasH9;
	wire[7:0] SecondBiasH0, SecondBiasH1, SecondBiasH2, SecondBiasH3, SecondBiasH4, SecondBiasH5, SecondBiasH6, SecondBiasH7, SecondBiasH8, SecondBiasH9;
	// Macs Output layer Bias Input
	wire[7:0] BiasO0, BiasO1, BiasO2, BiasO3, BiasO4, BiasO5, BiasO6, BiasO7, BiasO8, BiasO9;
	// Mac DataIn
	wire[7:0] DatasetOut, HregOut;
	wire[7:0] MacDataIn;
	// Hidden Layer Select
	wire[7:0] WeightHiddenSelect0, WeightHiddenSelect1, WeightHiddenSelect2, WeightHiddenSelect3, WeightHiddenSelect4, WeightHiddenSelect5, WeightHiddenSelect6, WeightHiddenSelect7, WeightHiddenSelect8, WeightHiddenSelect9;
	wire[7:0] BiasHiddenSelect0, BiasHiddenSelect1, BiasHiddenSelect2, BiasHiddenSelect3, BiasHiddenSelect4, BiasHiddenSelect5, BiasHiddenSelect6, BiasHiddenSelect7, BiasHiddenSelect8, BiasHiddenSelect9;
	// Mac Input Weight and Bias
	wire[7:0] MacInWeight0, MacInWeight1, MacInWeight2, MacInWeight3, MacInWeight4, MacInWeight5, MacInWeight6, MacInWeight7, MacInWeight8, MacInWeight9;
	wire[7:0] MacInBias0, MacInBias1, MacInBias2, MacInBias3, MacInBias4, MacInBias5, MacInBias6, MacInBias7, MacInBias8, MacInBias9;
	// Mac Output
	wire[7:0] MacOut0, MacOut1, MacOut2, MacOut3, MacOut4, MacOut5, MacOut6, MacOut7, MacOut8, MacOut9;
	// argmaxOut
	wire[3:0] argMaxOut;
	// Controller Instantiation
	controller CU (clk, rst, system_start, system_ready, WH_offset, WO_offset, select_wh, select_ho, select_data, load1, load2, load_out, ldmac, clr);
	// ROM Data
	DataReg DREG (.dataset_select(dataset_select), .offset(WH_offset), .data(DatasetOut) );
	// ROM Weight Hidden Layer
	WeightHReg WHREG (WH_offset ,FirstWeightH0, FirstWeightH1, FirstWeightH2, FirstWeightH3, FirstWeightH4, FirstWeightH5, FirstWeightH6, FirstWeightH7, FirstWeightH8, FirstWeightH9,
	SecondWeightH0, SecondWeightH1, SecondWeightH2, SecondWeightH3, SecondWeightH4, SecondWeightH5, SecondWeightH6, SecondWeightH7, SecondWeightH8, SecondWeightH9);
	// ROM Weight Output Layer
	WeightOReg WOREG (WO_offset ,WeightO0, WeightO1, WeightO2, WeightO3, WeightO4, WeightO5, WeightO6, WeightO7, WeightO8, WeightO9);
	// ROM Bias Hidden Layer
	BiasHReg BHREG (FirstBiasH0, FirstBiasH1, FirstBiasH2, FirstBiasH3, FirstBiasH4, FirstBiasH5, FirstBiasH6, FirstBiasH7, FirstBiasH8, FirstWBiasH9,
	SecondBiasH0, SecondBiasH1, SecondBiasH2, SecondBiasH3, SecondBiasH4, SecondBiasH5, SecondBiasH6, SecondBiasH7, SecondBiasH8, SecondBiasH9);
	// ROM Bias Output Layer
	BiasOReg BOREG (BiasO0, BiasO1, BiasO2, BiasO3, BiasO4, BiasO5, BiasO6, BiasO7, BiasO8, BiasO9);
	// ArgMax Module
	argmax ARGMAX (MacOut0, MacOut1, MacOut2, MacOut3, MacOut4, MacOut5, MacOut6, MacOut7, MacOut8, MacOut9, argMaxOut);
	// Output Register
	OutReg OREG (.out(outNetwork) , .parIn(argMaxOut) , .ld(load_out) , .clk(clk), .rst(rst) );
	// Hreg Registers
	Hreg HREG (clk, rst, load1 , load2, WO_offset,
	MacOut0, MacOut1, MacOut2, MacOut3, MacOut4, MacOut5, MacOut6, MacOut7, MacOut8, MacOut9, HregOut);
	// Multiplexer Weight Hidden Layer Mac set select
	assign {WeightHiddenSelect0,WeightHiddenSelect1,WeightHiddenSelect2,WeightHiddenSelect3,WeightHiddenSelect4,WeightHiddenSelect5,WeightHiddenSelect6,WeightHiddenSelect7,WeightHiddenSelect8,WeightHiddenSelect9} = 
	select_wh ?
	{SecondWeightH0,SecondWeightH1,SecondWeightH2,SecondWeightH3,SecondWeightH4,SecondWeightH5,SecondWeightH6,SecondWeightH7,SecondWeightH8,SecondWeightH9}
	:
	{FirstWeightH0,FirstWeightH1,FirstWeightH2,FirstWeightH3,FirstWeightH4,FirstWeightH5,FirstWeightH6,FirstWeightH7,FirstWeightH8,FirstWeightH9};
	// Multiplexer Bias Hidden Layer Mac set select
	assign {BiasHiddenSelect0,BiasHiddenSelect1,BiasHiddenSelect2,BiasHiddenSelect3,BiasHiddenSelect4,BiasHiddenSelect5,BiasHiddenSelect6,BiasHiddenSelect7,BiasHiddenSelect8,BiasHiddenSelect9} =
	select_wh ?
	{SecondBiasH0,SecondBiasH1,SecondBiasH2,SecondBiasH3,SecondBiasH4,SecondBiasH5,SecondBiasH6,SecondBiasH7,SecondBiasH8,SecondBiasH9}
	:
	{FirstBiasH0,FirstBiasH1,FirstBiasH2,FirstBiasH3,FirstBiasH4,FirstBiasH5,FirstBiasH6,FirstBiasH7,FirstBiasH8,FirstWBiasH9};
	// Multiplexer Weight Hidden Or Output select
	assign {MacInWeight0,MacInWeight1,MacInWeight2,MacInWeight3,MacInWeight4,MacInWeight5,MacInWeight6,MacInWeight7,MacInWeight8,MacInWeight9} =
	select_ho ?
	{WeightO0,WeightO1,WeightO2,WeightO3,WeightO4,WeightO5,WeightO6,WeightO7,WeightO8,WeightO9}
	:
	{WeightHiddenSelect0,WeightHiddenSelect1,WeightHiddenSelect2,WeightHiddenSelect3,WeightHiddenSelect4,WeightHiddenSelect5,WeightHiddenSelect6,WeightHiddenSelect7,WeightHiddenSelect8,WeightHiddenSelect9};
	// Multiplexer Bias Hidden Or Output select
	assign {MacInBias0,MacInBias1,MacInBias2,MacInBias3,MacInBias4,MacInBias5,MacInBias6,MacInBias7,MacInBias8,MacInBias9} =
	select_ho ?
	{BiasO0,BiasO1,BiasO2,BiasO3,BiasO4,BiasO5,BiasO6,BiasO7,BiasO8,BiasO9}
	:
	{BiasHiddenSelect0,BiasHiddenSelect1,BiasHiddenSelect2,BiasHiddenSelect3,BiasHiddenSelect4,BiasHiddenSelect5,BiasHiddenSelect6,BiasHiddenSelect7,BiasHiddenSelect8,BiasHiddenSelect9};
	// Multiplexer Mac Data In
	assign MacDataIn = select_data ? HregOut : DatasetOut;
	// Our 10 Hardware MACS
	
	SingleNeuron MAC0 (.weights(MacInWeight0) , .bios(MacInBias0), .data(MacDataIn), .out(MacOut0), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC1 (.weights(MacInWeight1) , .bios(MacInBias1), .data(MacDataIn), .out(MacOut1), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC2 (.weights(MacInWeight2) , .bios(MacInBias2), .data(MacDataIn), .out(MacOut2), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC3 (.weights(MacInWeight3) , .bios(MacInBias3), .data(MacDataIn), .out(MacOut3), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC4 (.weights(MacInWeight4) , .bios(MacInBias4), .data(MacDataIn), .out(MacOut4), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC5 (.weights(MacInWeight5) , .bios(MacInBias5), .data(MacDataIn), .out(MacOut5), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC6 (.weights(MacInWeight6) , .bios(MacInBias6), .data(MacDataIn), .out(MacOut6), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC7 (.weights(MacInWeight7) , .bios(MacInBias7), .data(MacDataIn), .out(MacOut7), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC8 (.weights(MacInWeight8) , .bios(MacInBias8), .data(MacDataIn), .out(MacOut8), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	SingleNeuron MAC9 (.weights(MacInWeight9) , .bios(MacInBias9), .data(MacDataIn), .out(MacOut9), .clk(clk), .rst(rst), .clr(clr), .ldmac(ldmac) );
	
endmodule
