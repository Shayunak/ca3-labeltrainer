module activationFunctionTB();
  reg ready;
  reg [10:0]inp;
  wire [10:0]out;
  
  activationFunction acf(.inp(inp), .out(out));
  defparam acf.m = 10;
  
  initial begin
    inp = 11'b01111111011;
    #100
    inp = 11'b10000011001;
    #100
    $stop;
  end
endmodule

module adderTB();
	reg[19:0] a,b;
	wire[19:0] s;
	adder adr (.a(a) , .b(b) , .s(s));
	defparam adr.len = 19;
	initial begin
	a = 1;
	b = 2;
	#100
	a = -a;
	b = b;
	#100
	$stop;
	end
endmodule

module MultTB();
	reg[7:0] A,B;
	wire[19:0] C;
	multiplier mult (.A(A) , .B(B) , .out(C) );
	defparam mult.accuSize = 19;
	initial begin
	#50
	A = 8'b10001110;
	B = 8'b11110000;
	#50
	$stop;
	end
endmodule

module argmaxTB();
  reg[7:0] data0, data1, data2, data3, data4, data5, data6, data7, data8, data9;
  wire[3:0] arg;
  argmax cut (data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, arg);
  initial begin
  #50
  data0=6; data1=7; data2=8; data3=9; data4=5; data5=4; data6=3; data7=2; data8=1; data9=0;
  #50
  data0=9; data1=8; data2=7; data3=6; data4=5; data5=4; data6=3; data7=2; data8=1; data9=0;
  #50
  data0=8; data1=9; data2=7; data3=6; data4=5; data5=4; data6=3; data7=2; data8=1; data9=0;
  #50
  data0=7; data1=8; data2=9; data3=6; data4=5; data5=4; data6=3; data7=2; data8=1; data9=0;
  #50
  data0=0; data1=4; data2=2; data3=4; data4=4; data5=3; data6=2; data7=7; data8=1; data9=0;
  #50
  data0=4; data1=5; data2=6; data3=7; data4=8; data5=250; data6=3; data7=2; data8=1; data9=0;
  #50
  data0=1; data1=2; data2=3; data3=4; data4=5; data5=6; data6=7; data7=8; data8=9; data9=0;
  #50
  data0=3; data1=4; data2=5; data3=6; data4=7; data5=8; data6=9; data7=2; data8=1; data9=0;
  #50
  data0=2; data1=3; data2=4; data3=5; data4=6; data5=7; data6=8; data7=9; data8=1; data9=0;
  #50
  data0=0; data1=1; data2=2; data3=3; data4=4; data5=5; data6=6; data7=7; data8=8; data9=9;
  #50
  $stop;
  end
endmodule

module counterTB();
	wire[5:0] val;
	reg[5:0] parIn;
	reg clk,rst,loadc;
	wire cout;
	counter cut (.loadc(loadc) , .parIn(parIn), .val(val), .cout(cout), .rst(rst), .clk(clk) );
	initial begin
		loadc = 1'b0;
		rst = 1'b1;
		#100
		rst = 1'b0;
    		clk = 1'b0;
    		repeat(1000) #100 clk = ~clk;
		$stop;  
  	end
	initial begin
		#400
		loadc = 1;
		parIn = 12;
		#100
		loadc = 0;		
		#300000
	$stop;
	end
endmodule

module controllerTB();
	reg system_start, clk, rst;
  	wire system_ready, select_wh, select_ho, select_data, load1, load2, load_out, ldmac, clr;
  	wire[5:0] WH_offset;
  	wire[4:0] WO_offset;
	controller cu (clk, rst, system_start, system_ready, WH_offset, 
		WO_offset, select_wh, select_ho, select_data, load1, load2, load_out, ldmac, clr);
	initial begin
		system_start = 1'b0;
		rst = 1'b0;
    		clk = 1'b0;
		#100
		rst = 1'b1;
		#100
		rst = 1'b0;
    		repeat(10000) #100 clk = ~clk;
		$stop;  
  	end
	initial begin
		#500
		system_start = 1'b1;
		#200
		system_start = 1'b0;
	end
endmodule

module HregTB();
	integer i;
	reg load1,load2,clk,rst;
	reg[4:0] offset;
	wire[7:0] out;
	reg[7:0] data0, data1, data2, data3, data4, data5, data6, data7, data8, data9;  
	Hreg hreg (clk, rst, load1 , load2, offset, data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, out);
	initial begin
		rst = 1'b0;
    		clk = 1'b0;
		load1 = 1'b0;
		load2 = 1'b0;
		offset = 0;
		#200
		rst = 1'b1;
		#200
		rst = 1'b0;
    		repeat(1000) #100 clk = ~clk;
  	end
	initial begin
		data0=1;data1=2;data2=3;data3=4;data4=5;data5=6;data6=7;data7=8;data8=9;data9=10;
		#400
		load1 = 1'b1;
		#200
		load1 = 1'b0;
		data0=10;data1=9;data2=8;data3=7;data4=6;data5=5;data6=4;data7=3;data8=2;data9=1;
		#400
		load2 = 1'b1;
		#200
		load2 = 1'b0;
		#400
		for(i = 0 ; i < 20; i= i + 1) begin
			#200
			offset = i;
		end
		#1000
		$stop;
	end
endmodule

module BiasORegTB();
	wire[7:0] bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9;
	BiasOReg cut (bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9);
	initial begin
		#1000
		$stop;
	end
endmodule

module BiasHRegTB();
	wire[7:0] bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9,
	bias10, bias11, bias12, bias13, bias14, bias15, bias16, bias17, bias18, bias19;
	//
	BiasHReg cut (bias0, bias1, bias2, bias3, bias4, bias5, bias6, bias7, bias8, bias9,
	bias10, bias11, bias12, bias13, bias14, bias15, bias16, bias17, bias18, bias19);
	//
	initial begin
		#1000
		$stop;
	end
endmodule

module WeightORegTB();
	integer i;
	reg[4:0] offset;
	wire[7:0] weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9; 
	WeightOReg woreg (offset ,weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9);
	initial begin
		for(i = 0 ; i < 20; i= i + 1) begin
			#200
			offset = i;
		end
		#1000
		$stop;
	end
endmodule

module WeightHRegTB();
	integer i;
	reg[5:0] offset;
	wire[7:0] weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9,
	weight10, weight11, weight12, weight13, weight14, weight15, weight16, weight17, weight18, weight19;
        // 
	WeightHReg whreg (offset ,weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9,
	weight10, weight11, weight12, weight13, weight14, weight15, weight16, weight17, weight18, weight19);
	//
	initial begin
		for(i = 0 ; i < 62; i= i + 1) begin
			#200
			offset = i;
		end
		#1000
		$stop;
	end
endmodule

module DataRegTB();
	integer i,j;
	reg[5:0] offset;
	reg[9:0] dataset_select;
	wire[7:0] data;
	DataReg dreg (dataset_select, offset, data);
	initial begin
		for(i = 0 ; i < 750 ; i = i + 1) begin
			for(j = 0 ; j < 62 ; j = j + 1) begin
				#200
				dataset_select = i;
				offset = j;
			end
		end
		#1000
		$stop;
	end
endmodule

module saturatoinTB();
  reg [21:0]inp;
  wire [7:0]out;
  
  saturation sat(.in(inp), .out(out));
  defparam sat.m = 21;
  
  initial begin
    inp = 22'b0111111101110101010101;
    #100
    inp = 22'b0000000000000000000111;
    #100
    $stop;
  end
endmodule
  
module singleNeuronTB();
  reg clk, rst,clr, ldmac, selectho;
  reg [7:0] weights, bios, data;
  wire [7:0]out;
  
  SingleNeuron mac (.clk(clk), .rst(rst), .weights(weights), .bios(bios), .data(data), .out(out), .clr(clr), .ldmac(ldmac), .selectho(selectho));
	defparam mac.accSize = 21;
	
	initial begin
		rst = 1'b0;
  		clk = 1'b0;
		#100
		rst = 1'b1;
		#100
		rst = 1'b0;
    		repeat(10000) #100 clk = ~clk;
		$stop;  
  	end
  	
	initial begin
	  ldmac = 1'b0;
	  clr = 1'b0;
	  selectho = 1'b0;
		#400
		/*weights = 8'b10010111;
	  bios = 8'b00000110;
	  data = 8'b11111111;
	  ldmac = 1'b1;
	  #200*/
		weights = 8'b01111111;
	  bios = 8'b01111110;
	  data = 8'b01111111;
	  ldmac = 1'b1;
	  #1000
	  weights = 8'b00101111;
	  bios = 8'b00000110;
	  data = 8'b00011111;
	  ldmac = 1'b1;
	  #200
	  ldmac = 0;
	end
	
endmodule