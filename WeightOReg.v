module WeightOReg(offset ,weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9);
	input[4:0] offset;
	output[7:0] weight0, weight1, weight2, weight3, weight4, weight5, weight6, weight7, weight8, weight9;
	reg[7:0]mem[0:199];
	initial begin
		$readmemh("./wo_sm.dat", mem);
	end
	//
	assign weight0 = mem[offset];
	assign weight1 = mem[offset + 20];
	assign weight2 = mem[offset + 40];
	assign weight3 = mem[offset + 60];
	assign weight4 = mem[offset + 80];
	assign weight5 = mem[offset + 100];
	assign weight6 = mem[offset + 120];
	assign weight7 = mem[offset + 140];
	assign weight8 = mem[offset + 160];
	assign weight9 = mem[offset + 180];
endmodule
