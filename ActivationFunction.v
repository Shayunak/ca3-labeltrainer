module activationFunction #(parameter m = 8)(inp, out);
  input [m:0]inp;
  output [m:0]out;
  
  assign out = (inp[m] == 0) ? inp : 0;
  
endmodule